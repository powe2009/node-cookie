const path = require('path');

const express = require('express');

const authController = require('../controllers/auth');

const router = express.Router();

// /admin/add-product => GET
router.get('/login-form', authController.getLoginForm);

module.exports = router;

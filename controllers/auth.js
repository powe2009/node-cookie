exports.getLoginForm = (req, res, next) => {
  res.render('auth/login-form', {
    pageTitle: 'Login Form',
    path: '/auth/login-form',
    editing: true
  });
};




